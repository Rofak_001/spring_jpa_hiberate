package com.spring_jpa_hibarnate.spring_jpa_hibarnate.Controller;

import com.spring_jpa_hibarnate.spring_jpa_hibarnate.Model.Employee;
import com.spring_jpa_hibarnate.spring_jpa_hibarnate.Model.Product;
import com.spring_jpa_hibarnate.spring_jpa_hibarnate.Model.Student;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.ldap.PagedResultsControl;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@RestController
public class StudentController {
    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @GetMapping("/post")
    public void getProduct() {
        EntityManager entityManager = sessionFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> criteriaQuery=criteriaBuilder.createQuery(Product.class);
        Root<Product> root=criteriaQuery.from(Product.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.greaterThan(root.get("proId"),2));
        List<Product> productsList=entityManager.createQuery(criteriaQuery).getResultList();
        for(int i=0;i<productsList.size();i++){
            System.out.println("Product ID:"+productsList.get(i).getProId());
            System.out.println("Product Name:"+productsList.get(i).getProName());
        }
    }
}
