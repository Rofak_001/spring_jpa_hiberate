package com.spring_jpa_hibarnate.spring_jpa_hibarnate.Model;

import javax.persistence.*;

@Entity
@Table(name = "\"tblEmp\"")
public class Employee {
    @Id
    @GeneratedValue
    @Column(name = "\"empId\"")
    private int empId;
    @Column(name = "\"empName\"")
    private String empName;
    public Employee(){}

    public Employee(int empId, String empName) {
        this.empId = empId;
        this.empName = empName;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }
}
