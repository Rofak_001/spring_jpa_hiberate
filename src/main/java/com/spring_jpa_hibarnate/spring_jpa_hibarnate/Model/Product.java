package com.spring_jpa_hibarnate.spring_jpa_hibarnate.Model;

import javax.persistence.*;

@Entity
@Table(name = "\"tblProduct\"")
public class Product {
    @Id
    @GeneratedValue
    @Column(name = "pro_id")
    private int proId;
    @Column(name = "pro_name")
    private String proName;

    public Product(){};

    public Product(int proId, String proName) {
        this.proId = proId;
        this.proName = proName;
    }

    public int getProId() {
        return proId;
    }

    public void setProId(int proId) {
        this.proId = proId;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }
}
