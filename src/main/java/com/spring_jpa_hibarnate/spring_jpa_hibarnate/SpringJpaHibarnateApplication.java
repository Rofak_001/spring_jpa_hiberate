package com.spring_jpa_hibarnate.spring_jpa_hibarnate;

import com.spring_jpa_hibarnate.spring_jpa_hibarnate.Model.Employee;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityManager;

@SpringBootApplication
public class SpringJpaHibarnateApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringJpaHibarnateApplication.class, args);
    }

}
